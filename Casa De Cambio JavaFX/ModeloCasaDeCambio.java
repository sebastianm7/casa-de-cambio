
import javax.swing.JOptionPane;

/**
 * Un ejemplo que modela una Casa de Cambio de BolÃ­vares usando POO
 *
 * @author (Milton JesÃºs Vera Contreras)
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE) History:
 * Abril 2008 / Marzo 2018
 */
public class ModeloCasaDeCambio {

    float precioDeCompra;
    float precioDeVenta;
    int bolivaresComprados;
    int bolivaresVendidos;
    float bolivaresEnCaja;
    float pesosEnCaja;

    //No requiere propiedades diferentes a las anteriores... No declare propiedades adicionales...
    /**
     * Default constructor
     */
    
    public ModeloCasaDeCambio() {
    }
    //COMPLETE GET 
    //No tiene sentido tener SET... piense, pregunte...
    public float getPrecioDeCompra() {
        return precioDeCompra;
    }

    public float getPrecioDeVenta() {
        return precioDeVenta;
    }

    public int getBolivaresComprados() {
        return bolivaresComprados;
    }

    public int getBolivaresVendidos() {
        return bolivaresVendidos;
    }

    public float getBolivaresEnCaja() {
        return bolivaresEnCaja;
    }

    public float getPesosEnCaja() {
        return pesosEnCaja;
    }

    /**
     * Calcula la ganancia de comprar un bolivar, es decir, la diferencia entre
     * los previos de compra y venta...
     */
    public float getGananciaEnUnBolivar() {
        float gananciaEnUnBolivar = precioDeVenta-precioDeCompra;
        return gananciaEnUnBolivar; //COMPLETE
    }

    /**
     * Es similar a los metodos SET, pero en este caso cambia al tiempo los dos
     * precios...por eso no hay SET Debe controlar que el precio no sea cero ni
     * negativo y que los precios generen ganancias...
     *
     * @param precioDeCompra
     * @param precioDeVenta
     * @return regresa true cuando pudo cambiar ambos precios, en caso contrario
     * falla.
     */
    public boolean cambiarPrecioDelBolivar(float precioDeCompra, float precioDeVenta) {
        boolean precioValido = false;//COMPLETE
        if (precioDeVenta > precioDeCompra && precioDeCompra > 0) {
            precioValido = true;
            this.precioDeVenta = precioDeVenta;
            this.precioDeCompra = precioDeCompra;
        }
        return precioValido;
    }//fin cambiarPrecioDelBolivar

    /**
     * Registra la compra de bolivares
     *
     * @param cantidad La cantidad de bolivares a comprar
     * @return true si pudo comprar
     */
    public boolean comprarBolivares(int cantidad) {
        boolean puedeComprar = false;//COMPLETE
        if (this.pesosEnCaja >= this.precioDeCompra*cantidad){
            puedeComprar = true;
            this.bolivaresComprados+=cantidad;
            this.pesosEnCaja-=this.precioDeCompra*cantidad;
            this.bolivaresEnCaja+=cantidad;
        }
        
        return puedeComprar;
    }//fin comprarBolivares

    /**
     *
     * @param cantidad
     * @return
     */
    public boolean venderBolivares(int cantidad) {
        boolean puedeVender = false;//COMPLETE
        if (this.bolivaresEnCaja>=cantidad) {
            puedeVender = true;
            this.bolivaresEnCaja-=cantidad;
            this.pesosEnCaja+=this.precioDeVenta*cantidad;
            this.bolivaresVendidos+=cantidad;
        }
        return puedeVender;
    }//fin venderBolivares

    /**
     * Calcula y regresa los impuestos, aunque no exista una propiedad llamada
     * impuestos, no se necesita...
     *
     * @return los impuestos, el 16% de los bolivares vendidos, convirtiendo a
     * pesos
     */
    public float getImpuestos() {
        float bolivar;
        bolivar=this.bolivaresVendidos*this.precioDeVenta;
        return bolivar*0.16f;//COMPLETE
    }//fin getImpuestos

    /**
     * Calcula y regresa las ganancias, aunque no exista una propiedad llamada
     * ganancias, no se necesita...
     *
     * @return las ganancias, que corresponden al dinero en pesos en caja menos
     * los impuestos
     */
    public float getGanancias() {
        float bolivar;
        bolivar=this.bolivaresVendidos*this.precioDeVenta;
        return bolivar - this.getImpuestos();//COMPLETE
    }//fin getGanancias

    /**
     * Aumenta la cantidad de pesos en caja, inyecta dinero al negocio
     *
     * @param cantidad Debe validarse que la cantidad no sea negativa...
     */
    public void inyectarPesos(int cantidad) {
        if (cantidad >= 0) {
            this.pesosEnCaja += cantidad;
        }
    }//fin inyectarPesos

    /**
     * Lo mismo que el anterior, pero con bolivares...
     *
     * @param cantidad
     */
    public void inyectarBolivares(int cantidad) {
        if (cantidad > 0) {
            this.bolivaresEnCaja += cantidad;
        }
    }
}//End class

