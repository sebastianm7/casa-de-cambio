import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javax.swing.JOptionPane;
import javafx.scene.image.ImageView;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import javafx.application.Application; 
import javafx.scene.Scene; 
import javafx.scene.layout.*; 
import javafx.event.ActionEvent; 
import javafx.event.EventHandler; 
import javafx.scene.control.*; 
import javafx.stage.Stage; 
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextInputDialog;


public class ControladorCasaDeCambio {

    @FXML
    private Label lblPrecioBsCompra;

    @FXML
    private Button cmdInyectarPesos;

    @FXML
    private BorderPane panel1;

    @FXML
    private Label lblGananciaBs;

    @FXML
    private Label lblTitulo;

    @FXML
    private Label lblImpuestos;

    @FXML
    private GridPane panelDatos;

    @FXML
    private Button cmdVenderBs;

    @FXML
    private Label txtImpuestos;

    @FXML
    private Label txtPesosCaja;
    
     @FXML
    private ImageView imageCDC;
    
    @FXML
    private Label lblBsCaja;

    @FXML
    private Label txtPrecioBsVenta;

    @FXML
    private Button cmdInyectarBs;

    @FXML
    private Label lblPesosCaja;

    @FXML
    private Label txtBsCaja;

    @FXML
    private Button cmdComprarBs;

    @FXML
    private Label lblBsComprados;

    @FXML
    private Button cmdCambiarBs;

    @FXML
    private Label txtBsComprados;

    @FXML
    private Button cmdAyuda;

    @FXML
    private Label txtBsVendidos;

    @FXML
    private Label txtGananciaBs;

    @FXML
    private Label lblGanancias;

    @FXML
    private Label txtPrecioBsCompra;

    @FXML
    private GridPane panelBotones;

    @FXML
    private Label lblBsVendidos;

    @FXML
    private Label txtGanancias;

    @FXML
    private Label lblPrecioBsVenta;
    
    private ModeloCasaDeCambio casaDeCambio;
    
    public ControladorCasaDeCambio(){
    casaDeCambio = new ModeloCasaDeCambio();
    }
    
    @FXML
    void cambiarPrecioBs() {
    TextInputDialog pc = new TextInputDialog("");
    pc.setTitle(null);
    pc.setHeaderText(null);
    pc.setContentText("Precio de Compra:");
    
    Optional<String> result = pc.showAndWait();
    
    TextInputDialog pv = new TextInputDialog("");
    pv.setTitle(null);
    pv.setHeaderText("Ingrese el Precio de Venta");
    pv.setContentText("Precio de Venta:");
    
    Optional<String> res = pv.showAndWait();
    
    float precioCompra = Float.parseFloat(result.get());
    float precioVenta = Float.parseFloat(res.get());
        
        
    /*float precioCompra = Float.parseFloat(JOptionPane.showInputDialog("Precio de Compra"));    
    float precioVenta = Float.parseFloat(JOptionPane.showInputDialog("Precio de Venta"));*/    
    if(casaDeCambio.cambiarPrecioDelBolivar(precioCompra, precioVenta)){
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "Registrado el cambio de precio");
    alert.showAndWait();
    txtPrecioBsCompra.setText(Float.toString(casaDeCambio.getPrecioDeCompra()));
    txtPrecioBsVenta.setText(Float.toString(casaDeCambio.getPrecioDeVenta()));
    txtGananciaBs.setText(Float.toString(casaDeCambio.getGananciaEnUnBolivar()));
    }else{ 
    Alert alert = new Alert(Alert.AlertType.ERROR, "Hay un error en el precio");
    alert.showAndWait();}
        
    /*casaDeCambio.precioDeCompra = Float.parseFloat(JOptionPane.showInputDialog("Precio de Compra"));
    casaDeCambio.precioDeVenta = Float.parseFloat(JOptionPane.showInputDialog("Precio de Venta"));
    if(casaDeCambio.cambiarPrecioDelBolivar(casaDeCambio.precioDeCompra, casaDeCambio.precioDeVenta)){
    txtPrecioBsCompra.setText(""+casaDeCambio.precioDeCompra);
    txtPrecioBsVenta.setText(""+casaDeCambio.precioDeVenta);
    txtGananciaBs.setText(""+casaDeCambio.getGananciaEnUnBolivar());
    JOptionPane.showMessageDialog(null, "Registrado el cambio de precio");
    }else JOptionPane.showMessageDialog(null, "Hay un error en el precio");*/
    }

    @FXML
    void inyectarPesos() {
    TextInputDialog in = new TextInputDialog("");
    in.setTitle(null);
    in.setHeaderText("Ingrese cantidad de pesos");
    in.setContentText("Cuantos pesos quiere inyectar?:");
    
    Optional<String> result = in.showAndWait();
    
    int inyectar = Integer.parseInt(result.get());
    
    casaDeCambio.inyectarPesos(inyectar);
    txtPesosCaja.setText(Float.toString(casaDeCambio.pesosEnCaja));
    txtGanancias.setText(Float.toString(casaDeCambio.getGanancias()));
    }

    @FXML
    void comprarBs() {
    TextInputDialog comprar = new TextInputDialog("");
    comprar.setTitle(null);
    comprar.setHeaderText("Cantidad de Bolívares a comprar");
    comprar.setContentText("Cuantos bolívares tiene?:");
    
    Optional<String> result = comprar.showAndWait();
    
    int comprarBs = Integer.parseInt(result.get());
    
    if(casaDeCambio.comprarBolivares(comprarBs)){;
    txtBsComprados.setText(Integer.toString(casaDeCambio.bolivaresComprados));
    txtPesosCaja.setText(Float.toString(casaDeCambio.pesosEnCaja));
    txtBsCaja.setText(Float.toString(casaDeCambio.bolivaresEnCaja));
    }else{
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "No hay pesos suficientes para comprar bolívares");
    alert.showAndWait();
    }
    }
    
    @FXML
    void inyectarBs() {
    TextInputDialog inBs = new TextInputDialog("");
    inBs.setTitle(null);
    inBs.setHeaderText("Cantidad de Bolívares a inyectar");
    inBs.setContentText("Cuantos bolívares?:");
    
    Optional<String> result = inBs.showAndWait(); 
    
    int cantidad = Integer.parseInt(result.get());   
    casaDeCambio.inyectarBolivares(cantidad);
    txtBsCaja.setText(Float.toString(casaDeCambio.getBolivaresEnCaja()));
    }

    @FXML
    void venderBs() {
    TextInputDialog vender = new TextInputDialog("");
    vender.setTitle(null);
    vender.setHeaderText("Cantidad de Bolívares a vender");
    vender.setContentText("Cuantos bolívares quiere?:");
    
    Optional<String> result = vender.showAndWait();    
    int cantidad = Integer.parseInt(result.get());   
    casaDeCambio.venderBolivares(cantidad);
    txtBsCaja.setText(Float.toString(casaDeCambio.getBolivaresEnCaja()));
    txtPesosCaja.setText(Float.toString(casaDeCambio.getPesosEnCaja()));
    txtBsVendidos.setText(Integer.toString(casaDeCambio.getBolivaresVendidos()));
    txtImpuestos.setText(Float.toString(casaDeCambio.getImpuestos()));
    txtGanancias.setText(Float.toString(casaDeCambio.getGanancias()));
    }

    @FXML
    void ayuda() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION, "Sistema de manejo de Casa de Cambios");
    alert.showAndWait();
    }

}
